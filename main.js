// Variáveis DOM
const column0 = document.getElementById('0');
const column1 = document.getElementById('1');
const column2 = document.getElementById('2');
const column3 = document.getElementById('3');
const column4 = document.getElementById('4');
const column5 = document.getElementById('5');
const column6 = document.getElementById('6');
const buttonReset = document.getElementById('reset');
const messages = document.getElementById('msg');
const scoreP1 = document.querySelector('#p1 p');
const scoreP2 = document.querySelector('#p2 p');
const divResult = document.getElementById('result')
const messageTimer = 3000;
const msgDraw = 'O jogo empatou';
const msgError = 'Escolha outra torre!';
const msgVictory = 'venceu o jogo!'
const maxLine = 6;
const musicButton = document.getElementById('music-control');
const musicPlayer = document.getElementById('music');
const currentDisk = document.getElementById('currentPlayer');
currentDisk.classList.add('play1');
let messageCode;
let currentPlayer = 1;
let endCondition;
let victoryCountP1 = 0;
let victoryCountP2 = 0;
let musicStatus = 'play';
let initialBoard = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
];
const axisX = initialBoard[0].length - 3;
const axisY = initialBoard.length - 3;

// Funções
// Faz a troca do player
const changePlayer = (disk) => {
    if (currentPlayer === 1) {
        currentDisk.classList.remove('play1');
        currentDisk.classList.add('play2');
        disk.classList.add('play1');
        currentPlayer = 2;
    } else {
        currentDisk.classList.remove('play2');
        currentDisk.classList.add('play1');
        disk.classList.add('play2');
        currentPlayer = 1;
    }
}

// Reseta jogo
const reset = () => {
    column0.innerHTML = '';
    column1.innerHTML = '';
    column2.innerHTML = '';
    column3.innerHTML = '';
    column4.innerHTML = '';
    column5.innerHTML = '';
    column6.innerHTML = '';
    // Retorna para o pássaro vermelho!
    if (currentPlayer === 2) {
        currentDisk.classList.remove('play2');
        currentDisk.classList.add('play1');
        currentPlayer = 1;
    }
    initialBoard = [
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0]
    ];
}

// Emite o recado em caso de vitória, empate ou erro de validação
const message = (msg, type) => {
    // Code 1: Vitória
    if (messageCode === 1) {
        let winner = currentPlayer;
        setTimeout(function () {
            divResult.innerText = `O jogador ${winner} ${msg}`;
            messages.classList.add(`${type}`);
            divResult.style.visibility = 'visible';
        }, 20);
        setTimeout(function () {
            divResult.innerHTML = "<br>";
            messages.classList.remove(`${type}`);
            divResult.style.visibility = 'hidden';
        }, messageTimer);
    }

    // Code 2: Empate
    if (messageCode === 2) {
        setTimeout(function () {
            divResult.innerText = `${msg}`;
            messages.classList.add(`${type}`);
            divResult.style.visibility = 'visible';
        }, 20);
        setTimeout(function () {
            divResult.innerHTML = "<br>";
            messages.classList.remove(`${type}`);
            divResult.style.visibility = 'hidden';
        }, messageTimer);
    }

    // Code 3: Erro
    if (messageCode === 3) {
        setTimeout(function () {
            messages.innerHTML = `${msg}`;
            messages.classList.add(`${type}`);
        }, 20);
        setTimeout(function () {
            messages.innerHTML = "<br>";
            messages.classList.remove(`${type}`);
        }, messageTimer);
    }
}

// Contador de Vitórias
const victoryCounter = () => {
    if (currentPlayer === 1) {
        victoryCountP1++;
        console.log(`victoryCountP1: ${victoryCountP1}`);
        scoreP1.innerHTML = `x ${victoryCountP1}`;
    } else {
        victoryCountP2++;
        console.log(`victoryCountP2: ${victoryCountP2}`);
        scoreP2.innerHTML = `${victoryCountP2} x`;
    }
}

// Condições de vitória e empate
// Horizontal
const horizontalWinCondition = () => {
    for (let y = 0; y < initialBoard.length; y++) {
        for (let x = 0; x < axisX; x++) {
            let cell = initialBoard[y][x];
            if (cell !== 0) {
                if (cell === initialBoard[y][x + 1] && cell === initialBoard[y][x + 2] && cell === initialBoard[y][x + 3]) {
                    messageCode = 1;
                    message(msgVictory, 'success');
                    endCondition = true;

                }
            }
        }
    }
}

// Vertical
const verticalWinCondition = () => {
    for (let y = 0; y < axisY; y++) {
        for (let x = 0; x < initialBoard[0].length; x++) {
            let cell = initialBoard[y][x];
            if (cell !== 0) {
                if (cell === initialBoard[y + 1][x] && cell === initialBoard[y + 2][x] && cell === initialBoard[y + 3][x]) {
                    messageCode = 1;
                    message(msgVictory, 'success');
                    endCondition = true;
                }
            }
        }
    }

}

// Diagonal Esquerda
const diagonalWinConditionL = () => {
    for (let y = 0; y < axisY; y++) {
        for (let x = 0; x < axisX; x++) {
            cell = initialBoard[y][x];
            if (cell !== 0) {
                if (cell === initialBoard[y + 1][x + 1] && cell === initialBoard[y + 2][x + 2] && cell === initialBoard[y + 3][x + 3]) {
                    messageCode = 1;
                    message(msgVictory, 'success');
                    endCondition = true;
                }
            }
        }
    }
}

// Diagonal Direita
const diagonalWinConditionR = () => {
    for (let y = 3; y < initialBoard.length; y++) {
        for (let x = 0; x < axisX; x++) {
            let cell = initialBoard[y][x]
            if (cell !== 0) {
                if (cell === initialBoard[y - 1][x + 1] && cell === initialBoard[y - 2][x + 2] && cell === initialBoard[y - 3][x + 3]) {
                    messageCode = 1;
                    message(msgVictory, 'success');
                    endCondition = true;
                }
            }
        }
    }
}

// Empate
const drawCondition = () => {
    let empate = column0.childElementCount === 6 && column1.childElementCount === 6 && column2.childElementCount === 6 && column3.childElementCount === 6 && column4.childElementCount === 6 && column5.childElementCount === 6 && column6.childElementCount === 6;

    if (empate) {
        messageCode = 2;
        message(msgDraw, 'draw');
        setTimeout(function () { reset(); }, messageTimer);
    }
}

// function que faz tudo rodar
const play = (event) => {
    let target = event.target;

    // validações
    if (target.classList.contains('disks')) {
        target = target.parentNode;
    }

    if (target.childElementCount > 5) {
        messageCode = 3;
        message(msgError, 'error');
        return;
    }

    let disk = document.createElement('div');
    disk.className = 'disks';
    target.appendChild(disk);

    let arrPosition = (maxLine - target.childElementCount) + ',' + target.id;
    arrPosition = arrPosition.split(',');
    initialBoard[arrPosition[0]][arrPosition[1]] = currentPlayer;

    endCondition = false;

    horizontalWinCondition();
    verticalWinCondition();
    diagonalWinConditionR();
    diagonalWinConditionL();

    if (endCondition) {
        victoryCounter();
        setTimeout(function () { reset(); }, messageTimer);
    }

    drawCondition();

    changePlayer(disk);
}

const hideInstructions = (instruction) => {
    document.getElementById('instructions').style.display = 'none';
    }

// Eventos
column0.addEventListener('click', play);
column1.addEventListener('click', play);
column2.addEventListener('click', play);
column3.addEventListener('click', play);
column4.addEventListener('click', play);
column5.addEventListener('click', play);
column6.addEventListener('click', play);
buttonReset.addEventListener('click', reset);
musicButton.addEventListener('click', function(){
    if (musicStatus === 'play') {
        musicPlayer.play();
        musicButton.classList.remove('play-music');
        musicButton.classList.add('pause-music');
        musicStatus = 'stop';
    } else {
        musicPlayer.pause();
        musicButton.classList.remove('pause-music');
        musicButton.classList.add('play-music');
        musicStatus = 'play';
    }
});
